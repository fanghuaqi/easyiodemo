#ifndef __GPIO_H__
#define __GPIO_H__

void init_gpio_func(void);
void activate_sim900(void);
void reset_sim900(void);
void led_ctl(int index , char status);
void relay_gpio_ctl(int index , char status);

#endif
